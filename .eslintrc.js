const OFF = 0;
const ERROR = 2;

module.exports = {
  'extends': ['standard', 'standard-preact'],
  'rules': {
    'indent': OFF,
    'no-tabs': OFF,
    'padded-blocks': OFF,
    'no-trailing-spaces': OFF,
    'no-unused-vars': OFF,
    'no-undef': OFF,
    'no-extra-boolean-cast': OFF,
    'import/first': OFF,
    'semi': OFF // turning off this annoyance.
  }
}
