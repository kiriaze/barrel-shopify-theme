__webpack_public_path__ = BRRL_PATH(BRRL_PUBLIC_PATH); // eslint-disable-line camelcase

import init from 'lib/init';
import { set, unset, toggle, isMobile } from 'lib/util';

import ScrollReveal from 'scrollreveal';

document.addEventListener('DOMContentLoaded', () => {
	
	init({
		module: 'modules'
	}).mount();

	ScrollReveal().reveal('.article-list .article-card', {
		interval: 150,
		distance: '20%',
		origin: 'bottom'
		// mobile: false
	});

});
